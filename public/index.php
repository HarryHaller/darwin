<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Darwin</title>
    <script src="app.js"></script>
    <style>
      body {
        padding-top: 70px;
      }
      .chart {
        width: 100%;
        height: 240px;
        border: 1px solid #ced4da;
        border-radius: .25rem;
      }
    </style>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
  </head>
  <body>
    <div class="modal" id="catwalk-modal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5>Catwalk</h5>
            <button type="button" class="close" data-dismiss="modal">
              <span>&times;</span>
            </button>
          </div>
          <div class="modal-body" id="catwalk-canvas"></div>
        </div>
      </div>
    </div>
    <header>
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="#">Evolution</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <!--ul class="navbar-nav mr-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
            </li>
          </ul-->
        </div>
      </nav>
    </header>

    <main role="main">
      <div class="container" id="config-container">
        <form id="config-form">
          <div class="form-row">
            <div class="col-sm">
              <div class="form-group">
                <label for="input-min-joints">Min Joints</label>
                <input type="number" class="form-control" name="minJoints" id="input-min-joints" value="4" min="3" max="50">
              </div>
            </div>
            <div class="col-sm">
              <div class="form-group">
                <label for="input-max-joints">Max Joints</label>
                <input type="number" class="form-control" name="maxJoints" id="input-max-joints" value="8" min="3" max="50">
              </div>
            </div>
            <div class="col-sm">
              <div class="form-group">
                <label for="input-creatures">Creatures</label>
                <input type="number" class="form-control" name="creatures" id="input-creatures" value="50" min="2" max="1000" step="2">
              </div>
            </div>
          </div>
          <div class="form-row">
            <div class="col-sm"></div>
            <div class="col-sm"></div>
            <div class="col-sm">
              <button type="submit" class="btn btn-block btn-primary">Generate</button>
            </div>
          </div>
        </form>
      </div>

      <div class="container" id="simulation-container">
        <form>
          <div class="form-row">
            <div class="col-sm form-group">
              <label for="input-seconds">Seconds</label>
              <input type="number" class="form-control" name="seconds" id="input-seconds" value="10" min="1" max="60">
            </div>
            <div class="col-sm form-group" title="+/- (RANDOM * VARIATION) ^ POWER">
              <label for="input-variation">Property Variation / Power</label>
              <div class="input-group">
                <input type="number" class="form-control" name="variation" id="input-variation" value="0.01" min="0.001" max="0.2" step="0.001">
                <input type="number" class="form-control" name="variation-power" id="input-variation-power" value="1" min="1" max="10">
              </div>
            </div>
            <div class="col-sm form-group">
              <label for="input-mutation">Mutation Probability</label>
              <input type="number" class="form-control" name="mutation" id="input-mutation" value="0.05" min="0.001" max="0.2" step="0.001">
            </div>
          </div>
        </form>

        <form>
          <div class="form-row">
            <div class="col-sm form-group">
              <label for="input-terrain-steps">Segments</label>
              <input type="number" class="form-control" name="steps" id="input-terrain-segments" value="20" min="1" max="100">
            </div>
            <div class="col-sm form-group">
              <label for="input-terrain-width">Segment Width</label>
              <input type="number" class="form-control" name="steps" id="input-terrain-width" value="50" min="10" max="10000">
            </div>
            <div class="col-sm form-group">
              <label for="input-terrain-height">Height Variation</label>
              <input type="number" class="form-control" name="steps" id="input-terrain-height" value="25" min="0" max="100">
            </div>
          </div>
        </form>
        <form>
          <div class="form-row">
            <div class="col-sm form-group">
              <button id="button-terrain" type="button" class="btn btn-block btn-secondary">Regenerate Terrain</button>
            </div>
            <div class="col-sm form-group">
              <button id="button-run" type="button" class="btn btn-block btn-primary">Run</button>
            </div>
            <div class="col-sm form-group">
              <button id="button-run-once" type="button" class="btn btn-block btn-primary">Run Once</button>
            </div>
          </div>
        </form>
        <div class="form-row">
          <div class="col-md form-group">
            <label>Distance</label>
            <div><canvas id="canvas-distance" class="chart"></canvas></div>
          </div>
          <div class="col-md form-group">
            <label>Species</label>
            <div><canvas id="canvas-species" class="chart"></canvas></div>
          </div>
        </div>
        <div class="form-row">
          <div class="col form-group">
            <table id="table-creatures" class="table table-striped">
              <thead>
                  <tr>
                    <th>#</th>
                    <th>Species</th>
                    <th>Joints</th>
                    <th>Muscles</th>
                    <th>Age</th>
                    <th>Distance</th>
                  </tr>
                </thead>
              <tbody></tbody>
            </table>
          </div>
        </div>
      </div>
    </main>
  </body>
</html>