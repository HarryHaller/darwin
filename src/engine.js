const Matter = require('matter-js');

const create = function(puppet, terrain) {
    let engine = Matter.Engine.create();
    Matter.Events.on(engine, 'afterUpdate', function(event) {
        puppet.update(event.timestamp);
    });
    Matter.World.add(engine.world, puppet.getElements());
    Matter.Body.translate(terrain, {
        x: -terrain.bounds.min.x - 600,
        y: -terrain.bounds.min.y + 300
    });
    Matter.World.add(engine.world, terrain);
   
    return engine;
}

const clear = function(engine) {
    Matter.World.clear(engine.world);
    Matter.Engine.clear(engine);
}

exports.create = create;
exports.clear = clear;