const Matter = require('matter-js');

const getRandom = function(steps, width, height) {
    let depth = 100,
        min = -500,
        max = 0;
    vertices = [{
        x: min,
        y: depth
    }, {
        x: min,
        y: 0,
    }, {
        x: 200,
        y: 0,
    }];
    for (var n = 0; n <= steps; n++) {
        max = 200 + width + width * n;
        vertices.push({
            x: max,
            y: height * (Math.random() - 0.5)
        });
    }
    vertices.push({x: max, y: depth});

    return Matter.Bodies.fromVertices(0, 0, vertices, {
        collisionFilter: {
            category: 1,
        },
        isStatic: true,
        friction: 1,
        render: {
            fillStyle: '#7ac945',
            lineWidth: 0
        }
    });
}

exports.getRandom = getRandom;