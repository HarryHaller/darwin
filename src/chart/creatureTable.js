export default class CreatureTable {
    constructor(container, catwalk) {
        let self = this;
        let table = container.DataTable({
            bInfo : false,
            searching: false,
            lengthChange: false,
            pageLength: 10,
        }).on('click', 'tr', function() {
            let data = table.row(this).data();
            if (data !== undefined) {
                let creature = self.creatures[data[0]];
                catwalk.start(creature, self.terrain);
            }
        });
        this.table = table;
        this.terrain;
    }
    
    update(creatures, terrain) {
        let n = 0;
        this.table.clear();
        this.terrain = terrain;
        this.creatures = creatures;
        creatures.forEach(function(creature) {
            this.table.row.add([
                n++,
                creature.species,
                creature.joints.length,
                creature.muscles.length,
                creature.age,
                creature.fitness.toFixed(2),
            ]).draw(false);
        }, this);
    }
};