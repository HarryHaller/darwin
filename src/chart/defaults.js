require('chart.js');

// Lines
Chart.defaults.global.elements.line.borderWidth = 2;
Chart.defaults.global.elements.line.borderColor = 'rgb(0, 123, 255)';
Chart.defaults.global.elements.line.backgroundColor = 'rgb(0, 123, 255, 0.2)';
Chart.defaults.global.elements.line.fill = true;

// Rectangles
Chart.defaults.global.elements.rectangle.borderWidth = 0;
Chart.defaults.global.elements.rectangle.borderColor = 'rgb(0, 123, 255)';
Chart.defaults.global.elements.rectangle.backgroundColor = 'rgb(0, 123, 255)';

// Points
Chart.defaults.global.elements.point.radius = 0;
Chart.defaults.global.elements.point.borderWidth = 0;
Chart.defaults.global.elements.point.borderColor = 'rgb(0, 123, 255)';
Chart.defaults.global.elements.point.backgroundColor = 'rgb(0, 123, 255)';
