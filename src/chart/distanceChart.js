
export default class DistanceChart
{
    constructor(canvas, config = {}) {
        this.generation = 0;
        this.config = Object.assign({
            maxLength: 50,
        }, config);

        this.chart = new Chart(canvas, {
            type: 'line',
            data: {
                labels: [],
                datasets: [
                    {
                        label: 'Max',
                    },
                    {
                        label: 'Quartil',
                    },
                    {
                        label: 'Median',
                    }
                ],
            },
            options: {
                animation: false,
                responsive: true,
                maintainAspectRatio: false,
				title: {
					display: true,
					text: 'Distance',
				},
                legend: {
                    display: false
                },
                tooltips: {
                    enabled: true
                },
                scales: {
                    xAxes: [{
                        display: false
                    }],
                    yAxes: [{
                        display: true,
                        ticks : {  
                            min : 0
                        },
                    }],
                },
            },
        });
    }

    update(creatures) {
        let fitness = creatures.map(
            creature => creature.fitness.toFixed(2)
        );
        // Labels
        this.chart.data.labels.push(
            this.generation++
        );
        // Data
        this.chart.data.datasets[0].data.push(
            fitness[0]
        );
        this.chart.data.datasets[1].data.push(
            getQuartil(fitness)
        );
        this.chart.data.datasets[2].data.push(
            getMedian(fitness)
        );
        // Slice to latest X
        let maxLength = this.config.maxLength;
        this.chart.data.datasets.forEach(function(dataset) {
            dataset.data = dataset.data.slice(-maxLength);
        });
        this.chart.data.labels = this.chart.data.labels.slice(-maxLength);
        this.chart.update();
    }
};

const getMean = function(values) {
    return values.reduce(function(a, b) {
        return a + b;
    }, 0) / values.length;
};

const getMedian = function(values) {
    let n = Math.floor((values.length-1) / 2);
    return values[n];
};

const getQuartil = function(values) {
    let n = Math.floor((values.length-1) / 4);
    return values[n];
};