export default class SpeciesChart {
    constructor(container) {
        this.chart = new Chart(container, {
			type: 'horizontalBar',
			data: {
				datasets: [{
					data: [],
				}],
				labels: [],
			},
			options: {
                animation: false,
                responsive: true,
                maintainAspectRatio: false,
				title: {
					display: true,
					text: 'Current species',
				},
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            },
        });
    }
    update(creatures) {
        let sorted = this.sortSpecies(
            this.countSpecies(creatures)
        );
        let head = sorted.slice(0, 4);
        let tail = sorted.slice(4).map(function(d) {
            return d[1];
        });


        let data = [];
        let labels = [];

        head.forEach(function(d) {
            labels.push(d[0]);
            data.push(d[1]);
        });
        while (data.length < 4) {
            labels.push("");
            data.push(0);
        }
        labels.push('Rest');
        data.push(tail.length ? tail.reduce(function(a, b) {
            return a + b;
        }) : 0);
        this.chart.data.labels = labels;
        this.chart.data.datasets[0].data = data;
        this.chart.update();
    }

    countSpecies(creatures) {
        let count = {};
        creatures.forEach(function(creature) {
            if (!count[creature.species]) {
                count[creature.species] = 1;
            } else {
                count[creature.species]++;
            }
        });
        return count;
    }

    sortSpecies(count) {
        let sortable = [];
        for (let species in count) {
            sortable.push([species, count[species]]);
        }
        sortable.sort(function(a, b) {
            return b[1] - a[1];
        });
        return sortable;
    }
};