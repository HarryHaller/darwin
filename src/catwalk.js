const Matter = require('matter-js');
const Puppet = require('./puppet');
const Engine = require('./engine');
const Html = require('./html');

class Catwalk {
    constructor(container) {
        this.container = container;
        this.engine = null;
        this.runner = null;

        let self = this;
        Html.getCatwalkModal().on('hide.bs.modal', function() {
            Matter.Runner.stop(self.runner, self.engine);
            Matter.World.clear(self.engine.world);
            Matter.Engine.clear(self.engine);
            while (container.firstChild) {
                container.removeChild(container.firstChild);
            }
        });
    }

    start(creature, terrain) {
        Html.getCatwalkModal().modal();
        let puppet = Puppet.create(creature);
        this.engine = Engine.create(puppet, terrain);
        this.runner = Matter.Runner.create();

        let renderer = Matter.Render.create({
            element: this.container,
            engine: this.engine,
            options: {
                wireframes: false,
                hasBounds: true,
                width: 466,
                height: 320,
                background: '#98d2f3',
            },
        });
        Matter.Events.on(this.engine, 'beforeTick', function() {
            Matter.Bounds.shift(renderer.bounds, {
                x: puppet.getPosition().x - 233,
                y: 80,
            });
        });
        Matter.Render.run(renderer);
        Matter.Runner.run(
            this.runner,
            this.engine
        );
    }
}

exports.Catwalk = Catwalk;