'use strict';

const $ = require('jquery');
const Html = require('./html');
const Creature = require('./creature');
const Terrain = require('./terrain');
const Catwalk = require('./catwalk');

import './chart/defaults';
import CreatureTable from './chart/creatureTable';
import DistanceChart from './chart/distanceChart';
import SpeciesChart from './chart/speciesChart';

require('bootstrap');
require('bootstrap/dist/css/bootstrap.min.css');
require("datatables.net-bs4");

window.decomp = require('poly-decomp');

$(function() {
    let catwalk = new Catwalk.Catwalk(
        Html.getCatwalkCanvas()[0]
    );
    let worker = new Worker('./worker.js?' + (new Date()).getTime());
    let simulator = new Simulator(worker, [
        new DistanceChart(Html.getCanvasDistance()),
        new SpeciesChart(Html.getCanvasSpecies()),
        new CreatureTable(
            Html.getTableCreatures(),
            catwalk,
        ),
    ]);

    Html.getSimulationContainer().hide();

    // Config form submit
    Html.getConfigForm().submit(function() {
        simulator.populate(Creature.getRandom(
            +Html.getConfigInputCreatures().val(),
            +Html.getConfigInputMinJoints().val(),
            +Html.getConfigInputMaxJoints().val(),
        ));
        Html.getSimulationContainer().show();
        Html.getConfigContainer().hide();
        return false;
    });

    // Start / stop simulation
    Html.getSimulateButton().click(function() {
        this.innerHTML = simulator.toggle()
            ? 'Stop'
            : 'Run';
    });
    Html.getSimulateOnceButton().click(function() {
        simulator.runOnce();
    });
    Html.getTerrainButton().click(function() {
        simulator.regenerateTerrain(
            +Html.getInputTerrainSegments().val(),
            +Html.getInputTerrainWidth().val(),
            +Html.getInputTerrainHeight().val(),
        );
    });
    simulator.regenerateTerrain(
        +Html.getInputTerrainSegments().val(),
        +Html.getInputTerrainWidth().val(),
        +Html.getInputTerrainHeight().val(),
    );
});

class Simulator {
    constructor(worker, displays = []) {
        this.worker = worker;
        this.displays = displays;
        this.creatures = [];
        this.working = false;
        this.config = {
            creatures: 50,
        };

        // Register the worker callback
        let self = this;
        this.worker.onmessage = function(e) {
            self.onMessage(e);
        };
    }

    regenerateTerrain(segments, width, height) {
        console.log(arguments);
        this.terrain = Terrain.getRandom(segments, width, height);
    }

    onMessage(e) {
        this.creatures = e.data;
        this.creatures.sort(function(a, b) {
            return a.fitness < b.fitness ? 1 : -1;
        });
        this.redraw();
        if (this.working) {
            this.simulate();
        }
    }

    toggle() {
        this.working = !this.working;
        if (this.working) {
            this.simulate();
        }
        return this.working;
    }

    runOnce() {
        this.working = false;
        this.simulate();
    }

    populate(creatures) {
        this.creatures = creatures;
    }

    simulate() {
        this.evolve();
        this.worker.postMessage({
            seconds: +Html.getInputSeconds().val(),
            creatures: this.creatures,
            terrain: this.terrain,
        });
    }

    evolve() {
        let mutation = +Html.getInputMutation().val();
        let variation = +Html.getInputVariation().val();
        let variationPower = +Html.getInputVariationPower().val();

        let survivors = this.creatures.slice(0, this.creatures.length / 2);
        this.creatures = [];

        survivors.forEach(function(creature) {
            // this.creatures.push(Creature.getOffspring(creature, variation, variationPower, mutation));
            this.creatures.push(Creature.getOffspring(creature, variation, variationPower, mutation));
            this.creatures.push(creature);
            creature.age++;
        }, this);
    }

    redraw() {
        this.displays.forEach(
            display => display.update(this.creatures, this.terrain),
            this
        );
    }
}