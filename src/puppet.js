'use strict';

const Matter = require('matter-js');

const x = 0;
const y = 300;
const scale = 150;
const radius = 10;

const create = function(creature) {
    let offX = 0;
    let offY = Math.max(...creature.joints.map(function(joint) {
        offX += joint.x;
        return joint.y;
    }));
    offX /= creature.joints.length;

    let puppet = new Puppet(creature.speed);
    creature.joints.forEach(joint => puppet.addJoint(
        (joint.x - offX) * scale + x,
        (joint.y - offY) * scale + y - radius / 2,
    ));
    creature.muscles.forEach(muscle => puppet.addMuscle(
        muscle.jointA,
        muscle.jointB,
        muscle.strength,
        muscle.scale,
        muscle.offset,
    ));
    return puppet;
}

class Puppet {
    constructor(speed) {
        this.speed = (2 - speed) / 2;
        this.joints = [];
        this.muscles = [];
    }

    addJoint(x, y) {
        let joint = Matter.Bodies.circle(x, y, radius, {
            collisionFilter: {
                category: 2,
                mask: 1,
            },
            inertia: Infinity,
            friction: 1,
            mass: 1,
            render: {
                fillStyle: '#d40000',
                strokeStyle: '#790b0b',
                lineWidth: 2,
            },
        });
        this.joints.push(joint);
    }

    addMuscle(jointA, jointB, strength, scale, offset) {
        let muscle = new Muscle(
            this.joints[jointA],
            this.joints[jointB],
            strength,
            scale,
            offset,
        );
        this.muscles.push(muscle);
    }

    getPosition() {
        let x = 0;
        let y = 0;
        this.joints.forEach(function(joint) {
            x += joint.position.x;
            y += joint.position.y;
        });
        return {
            x: x / this.joints.length,
            y: y / this.joints.length,
        };
    }

    update(timestamp) {
        timestamp += timestamp * this.speed;
        this.muscles.forEach(
            muscle => muscle.update(timestamp)
        );
    }

    getElements() {
        return this.joints.concat(this.muscles.map(
            muscle => muscle.constraint
        ));
    }
}

class Muscle {
    constructor(jointA, jointB, strength, scale, offset) {
        this.constraint = Matter.Constraint.create({
            bodyA: jointA,
            bodyB: jointB,
            stiffness: strength / 10,
            damping: 0.1,
            render: {
                visible: true,
                lineWidth: 2,
                strokeStyle: '#790b0b',
                type: 'line',
            },
        });
        this.restLength = this.constraint.length;
        this.offset = offset * 1000;
        this.scale = scale;
    }

    update(timestamp) {
        timestamp += this.offset;
        let factor = 1 + this.scale * Math.sin(timestamp / 500) * 0.8;
        this.constraint.length = this.restLength * factor;
    }
}

exports.create = create;