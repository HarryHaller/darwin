const $ = require('jquery');

// Simulation
exports.getSimulationContainer = function() {
    return $('#simulation-container');
};
exports.getSimulateButton = function() {
    return $('#button-run');
};
exports.getSimulateOnceButton = function() {
    return $('#button-run-once');
};
exports.getCatwalkCanvas = function() {
    return $('#catwalk-canvas');
};
exports.getCatwalkModal = function() {
    return $('#catwalk-modal');
};

// Charts + tables
exports.getCanvasDistance = function() {
    return $('#canvas-distance');
};
exports.getCanvasSpecies = function() {
    return $('#canvas-species');
};
exports.getTableCreatures = function() {
    return $('#table-creatures');
};

// Config form
exports.getConfigContainer = function() {
    return $('#config-container');
};
exports.getConfigForm = function() {
    return $('#config-form');
};
exports.getConfigInputMinJoints = function() {
    return $('#input-min-joints');
};
exports.getConfigInputMaxJoints = function() {
    return $('#input-max-joints');
};
exports.getConfigInputCreatures = function() {
    return $('#input-creatures');
};

// Simulation form
exports.getInputSeconds = function() {
    return $('#input-seconds');
};
exports.getInputVariation = function() {
    return $('#input-variation');
};
exports.getInputVariationPower = function() {
    return $('#input-variation-power');
};
exports.getInputMutation = function() {
    return $('#input-mutation');
};
// Terrain form
exports.getInputTerrainSegments = function() {
    return $('#input-terrain-segments');
};
exports.getInputTerrainWidth = function() {
    return $('#input-terrain-width');
};
exports.getInputTerrainHeight = function() {
    return $('#input-terrain-height');
};
exports.getTerrainButton = function() {
    return $('#button-terrain');
};