const Matter = require('matter-js');
const Puppet = require('./puppet');
const Engine = require('./engine');

const FPS = 60;

onmessage = function(e) {
    let creatures = e.data.creatures;
    let terrain = e.data.terrain;
    let seconds = e.data.seconds;
    creatures.forEach(function(creature) {
        let puppet = Puppet.create(creature);
        let engine = Engine.create(puppet, terrain);
        for (let n = 0; n < FPS * seconds; n++) {
            Matter.Engine.update(engine, 1000 / FPS);
        }
        creature.fitness = puppet.getPosition().x;
        Engine.clear(engine, terrain);
    });
    postMessage(creatures);
}