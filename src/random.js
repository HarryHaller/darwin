'use strict';

class Variator {
    constructor(max, pow) {
        this.max = max;
        this.pow = pow;
    }

    vary(value) {
        let offset = Math.pow(Math.random(), this.pow) * 2 * this.max - this.max;
        return clamp(value + offset);
    }
}

const clamp = function(value, min = 0, max = 1) {
    return Math.min(max, Math.max(min, value));
}

const getNumber = function(min = 0, max = 1) {
    return Math.random() * (max - min) + min;
}
        
const getInteger = function(min, max) {
    return Math.floor(getNumber(min, max));
}

const shuffle = function(array) {
    for (let m = array.length - 1; m > 0; m--) {
        let n = getInteger(0, m + 1);
        [array[m], array[n]] = [array[n], array[m]];
    }
    return array;
}

const pick = function(array) {
    let n = Math.floor(Math.random() * array.length);
    return array[n];
}

exports.getNumber = getNumber;
exports.getInteger = getInteger;
exports.shuffle = shuffle;
exports.pick = pick;
exports.Variator = Variator;