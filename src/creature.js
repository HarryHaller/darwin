'use strict';

const random = require('./random');

var species = 0;

const getRandom = function(number, min, max) {
    let creatures = [];
    for (let n = 0; n < number; n++) {
        let joints = random.getInteger(min, max + 1);
        let creature = getEmpty(
            species++,
            random.getNumber()
        );
        for (let n = 0; n < joints; n++) {
            addJoint(creature);
        }
        creatures.push(creature);
    }
    return creatures;
}

const getOffspring = function(parent, variation, variationPower, mutation) {
    let variator = new random.Variator(
        variation, variationPower
    );
    let child = getEmpty(
        parent.species,
        variator.vary(parent.speed)
    );
    parent.joints.forEach(function(joint) {
        child.joints.push({
            x: variator.vary(joint.x),
            y: variator.vary(joint.y),
        });
    });
    parent.muscles.forEach(function(muscle) {
        child.muscles.push({
            jointA: muscle.jointA,
            jointB: muscle.jointB,
            strength: variator.vary(muscle.strength),
            offset: variator.vary(muscle.offset),
            scale: variator.vary(muscle.scale),
        });
    });
    if (Math.random() < mutation) {
        Math.random() < 0.5
            ? addJoint(child)
            : removeJoint(child);
        child.species = species++;
    }
    return child;
}

const getEmpty = function(species, speed, age = 0) {
    return {
        species: species,
        speed: speed,
        age: age,
        fitness: 0,
        joints: [],
        muscles: [],
    };
}

const addJoint = function(creature) {
    let joints = random.shuffle(
        Array.from(creature.joints.keys())
    );
    // Add up to 3 muscles
    let max = Math.min(
        random.getInteger(2, 4),
        joints.length
    );
    for (let n = 0; n < max; n++) {
        creature.muscles.push({
            jointA: joints[n],
            jointB: joints.length,
            strength: random.getNumber(),
            offset: random.getNumber(),
            scale: random.getNumber(),
        });
    }
    // Add joint
    creature.joints.push({
        x: random.getNumber(),
        y: random.getNumber(),
    });
}

const removeJoint = function(creature) {
    let joint = random.getInteger(0, creature.joints.length);
    let joints = creature.joints.length;

    // Remove joint
    creature.joints = [].concat(
        creature.joints.slice(0, joint),
        creature.joints.slice(joint + 1),
    );
    // Remove connected muscles
    creature.muscles = creature.muscles.filter(function(muscle) {
        return muscle.jointA !== joint
            && muscle.jointB !== joint;
    });
    // Adjust joint indices for remaining muscles
    for (let n = joint; n < joints; n++) {
        creature.muscles.forEach(function(muscle) {
            if (muscle.jointA >= n) {
                muscle.jointA--;
            }
            if (muscle.jointB >= n) {
                muscle.jointB--;
            }
        });
    }
}

const mirror = function(creature) {
    creature.joints.forEach(function(joint) {
        joint.x = 1 - joint.x;
    });
}

export {
    getRandom,
    getOffspring,
    mirror,
}