const path = require('path');

module.exports = {
    mode: 'development',
    devtool: 'inline-source-map',
    entry: {
        app: path.resolve(__dirname, 'src/app.js'),
        worker: path.resolve(__dirname, 'src/worker.js'),
    },
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: '[name].js'
    },
    watchOptions: {
        aggregateTimeout: 500,
        poll: 1000
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            }
        ]
    }
};